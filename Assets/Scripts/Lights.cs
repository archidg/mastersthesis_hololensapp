﻿using ArchiDemo.Inputs;

namespace ArchiDemo
{
    public class Lights : Singleton<Lights>
    {
        protected override void Start()
        {
            base.Start();

            VoiceInput.CommandRecognized += VoiceInput_CommandRecognized;
        }

        private void VoiceInput_CommandRecognized(object sender, VoiceInput.CommandRecognizedEventArgs e)
        {
            switch(e.Command)
            {
                case VoiceInput.VoiceCommand.Lights_On:
                    Activate();
                    break;

                case VoiceInput.VoiceCommand.Lights_Off:
                    Deactivate();
                    break;
            }
        }
    }
}