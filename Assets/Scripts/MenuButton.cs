﻿// Mohamed:Lightify

using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ArchiDemo
{
    public class MenuButton : Pressable   // inherits Pressable
    {
        private Image _image;
        private TextMeshProUGUI _text;

        private Color _normalBackColor;
        private Color _normalForeColor;
        private Color _focusedForeColor;
        private Color _focusedBackColor;
        private Color _disabledForeColor;
        private Color _disabledBackColor;    //private attributes denoted by "_" before the name

        protected override void Awake()
        {
            base.Awake();

            _image = GetComponent<Image>();
            _text = transform.Find("Text").GetComponent<TextMeshProUGUI>();

            BoxCollider collider = gameObject.GetComponent<BoxCollider>();
            RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
            collider.size = new Vector3(rectTransform.rect.width, rectTransform.rect.height, 2);

            _normalForeColor = _text.color;
            _normalBackColor = _image.color;

            _disabledForeColor = Color.Lerp(_normalForeColor, Color.gray, 0.9f);
            _disabledBackColor = Color.Lerp(_normalBackColor, Color.gray, 0.9f);

            _focusedForeColor = Color.Lerp(_normalForeColor, Color.white, 0.3f);
            _focusedBackColor = Color.Lerp(_normalBackColor, Color.white, 0.3f);
        }

        //protected override void Update()
        void Update()
        {
            // base.Update();

            if(IsEnabled)
            {
                if (HasFocus)
                {
                    _text.color = _focusedForeColor;
                    _image.color = _focusedBackColor;
                }
                else
                {
                    _text.color = _normalForeColor;
                    _image.color = _normalBackColor;
                }
            }
            else
            {
                _text.color = _disabledForeColor;
                _image.color = _disabledBackColor;
            }
        }
    }
}