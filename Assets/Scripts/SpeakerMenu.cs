﻿// ArchiDemo
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ArchiDemo
{
    // If singleton is used, only one instance will be created,
    // access within code by name.
    public class SpeakerMenu : Singleton<SpeakerMenu>    
    {
        private MenuButton _lowVolumeButton; // "_" used to convey that this is not a local variable
        private MenuButton _highVolumeButton; // "_" used to convey that this is not a local variable
        private MenuButton _mediumVolumeButton; // "_" used to convey that this is not a local variable

        // event-handler Represents the method that will handle an event when the event provides data.
        // in C#: class, method and event capital letters, 
        // only variable names starts with small letters. 
        public static event EventHandler LowVolumeButtonClicked;   
        public static event EventHandler HighVolumeButtonClicked;
        public static event EventHandler MediumVolumeButtonClicked;

        // Use this for initialization
        protected override void Start()
        {
            _lowVolumeButton = transform.Find("LowMenuButton").GetComponent<MenuButton>();  // find game object named "LowMenuButton" in the children,
                                                                                            // and assign it to the "_lowVolumeButton" variable
            _lowVolumeButton.Pressed += _lowVolumeButton_Pressed;   // '+=' + 'tab' creates the method signature for event handler

            _highVolumeButton = transform.Find("HighMenuButton").GetComponent<MenuButton>();
            _highVolumeButton.Pressed += _highVolumeButton_Pressed;

            _mediumVolumeButton = transform.Find("MediumMenuButton").GetComponent<MenuButton>();
            _mediumVolumeButton.Pressed += _mediumVolumeButton_Pressed;

        }

        /*internal string GetComponent(object meduimVolumeButton)
        {
            throw new NotImplementedException();
        }*/

        //  event-handler is a method that contains the code that gets executed
        // in response to a specific event that occurs in an application.
        private void _mediumVolumeButton_Pressed(object sender, EventArgs e)
        {

            EventHandler temp = MediumVolumeButtonClicked;// creating temp prevents it from calling 
                                                          // a null pointer exception, when the assignment changes
            if (temp != null) {
                temp.Invoke(this, EventArgs.Empty);

                //TextMeshProUGUI buttonText = _mediumVolumeButton.GetComponent<TextMeshProUGUI>();
                //String text = buttonText.text;
                //print("medium volume button pressed: " + text);
            }
        }

        //  event-handler is a method that contains the code that gets executed
        // in response to a specific event that occurs in an application.
        private void _highVolumeButton_Pressed(object sender, EventArgs e)
        {
            //print("high volume button pressed");
            EventHandler temp = HighVolumeButtonClicked; // creating temp prevents it from calling 
                                                         // a null pointer exception, when the assignment changes
            if (temp != null) {
                temp.Invoke(this, EventArgs.Empty);
            }
        }

        //  event-handler is a method that contains the code that gets executed
        // in response to a specific event that occurs in an application.
        private void _lowVolumeButton_Pressed(object sender, System.EventArgs e)
        {
            //print("low volume button pressed");
            EventHandler temp = LowVolumeButtonClicked;  // creating temp prevents it from calling a null pointer 
                                                         // exception, when the assignment changes
            if (temp != null) {
                temp.Invoke(this, EventArgs.Empty);
            }

        }

        public void ShowMenu(Vector3 position) {
            transform.position = position;  // the menu appears over the gameObject speaker
            gameObject.SetActive(true);  // show menu
        }

        public void HideMenu()
        {
            gameObject.SetActive(false);
        }

        private void Update()
        {
            gameObject.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
            // make sure the menu faces the user
        }

    }
}
