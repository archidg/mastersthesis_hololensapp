﻿using ArchiDemo.Inputs;
using UnityEngine;

namespace ArchiDemo
{
    public class UserHand : Singleton<UserHand>
    {
        public bool IsEmpty { get { return transform.childCount == 0; } }

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            HandInput.Updated += HandInput_Updated;
        }

        private void HandInput_Updated(object sender, HandInput.HoloLensHandEventArgs e)
        {
            transform.position = e.Position;
        }
    }
}