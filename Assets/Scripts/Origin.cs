﻿using ArchiDemo.Inputs;
using UnityEngine;

namespace ArchiDemo
{
    public class Origin : Singleton<Origin>
    {
        private bool _isMovable;

        protected override void Start()
        {
            base.Start();
            AlignToUser();
            _isMovable = false;
            SwitchToDesignMode();
            VoiceInput.CommandRecognized += VoiceInput_CommandRecognized;
        }

        // Update is called once per frame
        void Update()
        {
            if (_isMovable) AlignToUser();
        }

        private void AlignToUser()
        {
            Transform cameraTransform = Camera.main.transform;
            Vector3 cameraTransformPosition = cameraTransform.position;
            Quaternion cameraTransformRotation = cameraTransform.rotation;

            Quaternion rotation = cameraTransformRotation;
            rotation.x = 0;
            rotation.z = 0;
                
            Vector3 forwardPosition = cameraTransformPosition + cameraTransformRotation * Vector3.forward;

            transform.position = forwardPosition;
            transform.rotation = rotation;
        }

        // Real scale; 1:1
        private void SwitchToPreviewMode()
        {
            transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
        }

        private void SwitchToDesignMode()
        {
            transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }

        ///
        // NEW -- model scale 1:1
        private void SwitchToUserConfigMode() 
        {
            transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
        }

        // NEW -- model scale 1:1
        private void SwitchToDesignerConfigMode()
        {
            transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
        }

        // NEW -- model scale 1:1
        private void SwitchToInteractionConfigMode()
        {
            transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
        }

        // NEW -- Same as "SwitchToDesignMode()" -- small scale
        private void SwitchToArchitecturalDesignMode()
        {
            transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
      ///

        // call the functions based on voice command
        private void VoiceInput_CommandRecognized(object sender, VoiceInput.CommandRecognizedEventArgs e)
        {
            switch (e.Command)
            {
                case VoiceInput.VoiceCommand.Move: _isMovable = true; break;
                case VoiceInput.VoiceCommand.Stop: _isMovable = false; break;

                case VoiceInput.VoiceCommand.Design: SwitchToDesignMode(); break;
                case VoiceInput.VoiceCommand.Preview: SwitchToPreviewMode(); break;

                case VoiceInput.VoiceCommand.User_mode: SwitchToUserConfigMode(); break;
                case VoiceInput.VoiceCommand.Designer_mode: SwitchToDesignerConfigMode(); break;
                case VoiceInput.VoiceCommand.Interaction_mode: SwitchToDesignerConfigMode(); break;
            }
        }
    }
}