﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NEW CLASS
namespace ArchiDemo
{
    public class WorldCursor : MonoBehaviour
    {
        private MeshRenderer meshRenderer;

        // Use this for initialization
        void Start()
        {
            // grab the meshrenderer that's on the same object as this script
            meshRenderer = this.gameObject.GetComponentInChildren<MeshRenderer>();
        }

        // Update is called once per frame
        void Update()
        {
            // raycast on the world based on the user's head position and orientation
            var headPosition = Camera.main.transform.position;
            var gazeDirection = Camera.main.transform.forward;

            RaycastHit hitInfo;

            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo))
            {
                // if raycasr hit a hologram
                // display cursor mesh
                meshRenderer.enabled = true;

                // move the cursor to the point where raycast hit
                this.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);

            }
            else
            {
                // if cursor did not hit a hologram, hide curser mesh
                meshRenderer.enabled = false;
            }

        }
    }
}