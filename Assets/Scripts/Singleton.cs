﻿// Mohamed NurseAid
using System;
using UnityEngine;

namespace ArchiDemo
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        public static T Instance { get; private set; }

        /// <summary>
        /// Returns whether the instance is activated or not.
        /// </summary>
        public static bool IsActivated { get; private set; }

        /// <summary>
        /// Returns whether the instance has been initialized or not.
        /// </summary>
        public static bool IsInitialized { get { return Instance != null; } }

        static Singleton()
        {
            IsActivated = false;
        }

        /// <summary>
        /// Activates the instance once it is initialized;
        /// </summary>
        public static void Activate()
        {
            IsActivated = true;
            if (IsInitialized && Instance.StartCalled) Instance.gameObject.SetActive(IsActivated);
        }

        /// <summary>
        /// Deactivates the instance once it is initialized;
        /// </summary>
        public static void Deactivate()
        {
            IsActivated = false;
            if (IsInitialized && Instance.StartCalled) Instance.gameObject.SetActive(IsActivated);
        }

        /// <summary>
        /// Returns whether the Start method has been called or not.
        /// </summary>
        protected bool StartCalled { get; set; }

        /// <summary>
        /// Scripts that extend Singleton should be sure to call base.Awake().
        /// </summary>
        protected virtual void Awake()
        {
            StartCalled = false;
            if (!IsInitialized) Instance = (T)this;
            else throw new Exception("Trying to instantiate a second instance of singleton class '" + GetType().ToString() + "'.");
        }

        /// <summary>
        /// Scripts that extend Singleton should be sure to call base.Start().
        /// </summary>
        protected virtual void Start()
        {
            StartCalled = true;
            gameObject.SetActive(IsActivated);
        }

        /// <summary>
        /// Scripts that extend Singleton should be sure to call base.OnDestroy().
        /// </summary>
        protected virtual void OnDestroy()
        {
            if (Instance == this) Instance = null;
        }
    }
}