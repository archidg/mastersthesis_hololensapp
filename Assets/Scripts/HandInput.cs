﻿using System;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

namespace ArchiDemo.Inputs
{
    public class HandInput : Singleton<HandInput>
    {
        public class HoloLensHandEventArgs : EventArgs
        {
            public uint HandId { get; private set; }
            public Vector3 Position { get; private set; }

            public HoloLensHandEventArgs(uint handId, Vector3 position)
            {
                HandId = handId;
                Position = position;
            }
        }

        public static event EventHandler<HoloLensHandEventArgs> Lost;
        public static event EventHandler<HoloLensHandEventArgs> Updated;
        public static event EventHandler<HoloLensHandEventArgs> Detected;

        public static event EventHandler Pressed;
        public static event EventHandler Released;

        protected void OnEnable()
        {
            if (StartCalled)
            {
                InteractionManager.InteractionSourceLost += InteractionManager_InteractionSourceLost;
                InteractionManager.InteractionSourceUpdated += InteractionManager_InteractionSourceUpdated;
                InteractionManager.InteractionSourceDetected += InteractionManager_InteractionSourceDetected;

                InteractionManager.InteractionSourcePressed += InteractionManager_InteractionSourcePressed;
                InteractionManager.InteractionSourceReleased += InteractionManager_InteractionSourceReleased;
            }
        }

        protected void OnDisable()
        {
            InteractionManager.InteractionSourceLost -= InteractionManager_InteractionSourceLost;
            InteractionManager.InteractionSourceUpdated -= InteractionManager_InteractionSourceUpdated;
            InteractionManager.InteractionSourceDetected -= InteractionManager_InteractionSourceDetected;

            InteractionManager.InteractionSourcePressed -= InteractionManager_InteractionSourcePressed;
            InteractionManager.InteractionSourceReleased -= InteractionManager_InteractionSourceReleased;
        }

        private void InteractionManager_InteractionSourceLost(InteractionSourceLostEventArgs obj)
        {
            EventHandler<HoloLensHandEventArgs> temp = Lost;

            if (temp != null && obj.state.source.kind == InteractionSourceKind.Hand)
            {
                Vector3 position;
                if (obj.state.sourcePose.TryGetPosition(out position))
                {
                    temp.Invoke(this, new HoloLensHandEventArgs(obj.state.source.id, position));
                }
            }
        }

        private void InteractionManager_InteractionSourceUpdated(InteractionSourceUpdatedEventArgs obj)
        {
            EventHandler<HoloLensHandEventArgs> temp = Updated;

            if (temp != null && obj.state.source.kind == InteractionSourceKind.Hand)
            {
                Vector3 position;
                if (obj.state.sourcePose.TryGetPosition(out position))
                {
                    temp.Invoke(this, new HoloLensHandEventArgs(obj.state.source.id, position));
                }
            }
        }

        private void InteractionManager_InteractionSourceDetected(InteractionSourceDetectedEventArgs obj)
        {
            EventHandler<HoloLensHandEventArgs> temp = Detected;

            if (temp != null && obj.state.source.kind == InteractionSourceKind.Hand)
            {
                Vector3 position;
                if (obj.state.sourcePose.TryGetPosition(out position))
                {
                    temp.Invoke(this, new HoloLensHandEventArgs(obj.state.source.id, position));
                }
            }
        }

        private void InteractionManager_InteractionSourcePressed(InteractionSourcePressedEventArgs obj)
        {
            EventHandler temp = Pressed;

            if (temp != null) temp.Invoke(this, EventArgs.Empty);
        }

        private void InteractionManager_InteractionSourceReleased(InteractionSourceReleasedEventArgs obj)
        {
            EventHandler temp = Released;

            if (temp != null) temp.Invoke(this, EventArgs.Empty);
        }
    }
}