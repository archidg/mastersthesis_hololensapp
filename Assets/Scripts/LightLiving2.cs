﻿
// New test scripts for creating the behavior of interior light object
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightLiving2 : MonoBehaviour {

    private Light myLight;

	// Use this for initialization
	void Start () {
        myLight = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Space)) {
            myLight.enabled = !myLight.enabled; // toggle between light on/off
                                                // by hitting space bar
        }

        myLight.intensity = Mathf.Lerp(myLight.intensity, 8f, 0.5f);
        /**
         * The Mathf.Lerp function takes 3 float parameters: one representing 
         * the value to interpolate from; another representing the value to 
         * interpolate to and a final float representing how far to interpolate. 
         * In this case, the interpolation value is 0.5 which means 50%. 
         * If it was 0, the function would return the ‘from’ value and if it was 
         * 1 the function would return the ‘to’ value.
         **/

        // To get a axis between two buttons to control luminosity use the "GetAxis"
        // look at the GetAxis unity tutorial

        // Use "OnMouseDown" to make a gameObject clickable.

        // other useful methods are "GetComponent"
    }
}
