﻿using System;
using UnityEngine.Windows.Speech;

namespace ArchiDemo.Inputs
{
    public class VoiceInput : Singleton<VoiceInput>
    {
        public enum VoiceCommand
        {
            Move,
            Stop,

            Design, // "Architectural Design Mode" / model small scale
            Preview, // model scale 1:1

            User_mode, // model scale 1:1
            Designer_mode, // model scale 1:1
            Interaction_mode, // model scale 1:1

            Lights_On,
            Lights_Off
        }

        public class CommandRecognizedEventArgs : EventArgs
        {
            public VoiceCommand Command { get; private set; }

            public CommandRecognizedEventArgs(VoiceCommand command)
            {
                Command = command;
            }
        }

        public static event EventHandler<CommandRecognizedEventArgs> CommandRecognized;

        private string[] _keywords;
        private KeywordRecognizer _keywordRecognizer;

        protected override void Awake()
        {
            base.Awake();
            _keywords = Enum.GetNames(typeof(VoiceCommand));
            _keywordRecognizer = new KeywordRecognizer(_keywords);
        }

        void OnEnable()
        {
            if (StartCalled)
            {
                _keywordRecognizer.Start();
                _keywordRecognizer.OnPhraseRecognized += _keywordRecognizer_OnPhraseRecognized;
            }
        }

        void OnDisable()
        {
            if (_keywordRecognizer.IsRunning) _keywordRecognizer.Stop();
            _keywordRecognizer.OnPhraseRecognized -= _keywordRecognizer_OnPhraseRecognized;
        }

        protected override void OnDestroy()
        {
            _keywordRecognizer.Dispose();

            base.OnDestroy();
        }

        private void _keywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
        {
            EventHandler<CommandRecognizedEventArgs> temp = CommandRecognized;

            if (temp != null)
            {
                if (args.confidence == ConfidenceLevel.High || args.confidence == ConfidenceLevel.Medium)
                {
                    foreach (string keyword in _keywords)
                    {
                        if (keyword == args.text)
                        {
                            temp.Invoke(this, new CommandRecognizedEventArgs((VoiceCommand)Enum.Parse(typeof(VoiceCommand), keyword)));
                            break;
                        }
                    }
                }
            }
        }
    }
}