﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArchiDemo
{
    public enum ModuleType
    {
        Bathroom,
        Bedroom1,
        Bedroom2,
        Kitchen,
        Dining,
        Living1,
        Living2,
        Terrace
    }
}