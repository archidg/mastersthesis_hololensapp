﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NEW CLASS
namespace ArchiDemo
{
    // Types of smart objects
    public enum smartObjectType
    {
        Light, AC, Curtain, Couch, TV,

        MotionSensor, SmokeSensor, TemperatureSensor, IRSensor, ProximitySensor,

        Sprinkler, WaterLeakDetector, SmokeAlarm
    }

    // Context/Room info
    public enum Context {
        Roomname, TimeOfDay, User, Activity, SmartObject
    }

   
    // Trigger-Affect and activity of smart objects
    public class SmartObject : MonoBehaviour
    {
        public smartObjectType smartObjectType;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

    //class StudentName
    //{
      //  public string FirstName { get; set; }
      //  public string LastName { get; set; }
      //  public int ID { get; set; }
    //}

    // The class and properties of a smart object
    class SmartObj {
        public string RoomName { get; set; }
        public string Activity { get; set; }
        public string Trigger { get; set; }
        public string Affect { get; set; }

    }

    class CollInit
    {
        Dictionary<string, SmartObj> obj = new Dictionary<string, SmartObj>()
    {
        { "Light", new SmartObj {RoomName="LivingRoom", Activity="Movie", Trigger="Presence", Affect = "Illumination"}},
        //{ "Curtain", new SmartObj {RoomName="DiningRoom", Activity="Dinner", Trigger="Presence", Affect = "Illumination"}},
        //{ "BedRoom", new SmartObj {RoomName="BedRoom", Activity="Sleep", Trigger="Presence", Affect = "Illumination"}}
    };
    }
}
