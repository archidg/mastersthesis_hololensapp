﻿// Mohamed:lightify

using ArchiDemo.Inputs;
using System;
using UnityEngine;

namespace ArchiDemo
{
    [RequireComponent(typeof(BoxCollider))]
    public class Pressable : MonoBehaviour
    {
        public bool IsEnabled { get; set; }   // attributes of the pressable object  -- set isEnabled to true/false 

        public bool HasFocus { get; private set; }  // attributes of the pressable object  -- set HasFocus to true/false 

        public bool IsPressed { get; private set; } // attributes of the pressable object  -- set IsPressed to true/false 

        public event EventHandler Pressed;
        public event EventHandler Released;
        //public event EventHandler StayPressed;

        protected virtual void Awake()
        {
            IsEnabled = true;
            HasFocus = false;
        }

        //protected virtual void Update()
        //{
        //    if (IsPressed)
        //    {
        //        EventHandler temp = StayPressed;
        //        if (temp != null) temp.Invoke(this, EventArgs.Empty);
        //    }
        //}
        
        private void OnEnable()
        {
            HandInput.Pressed += HandInput_Pressed;
            HandInput.Released += HandInput_Released;
            GazeInput.GazeTargetChanged += GazeInput_GazeTargetChanged;
        }

        private void OnDisable()
        {
            HandInput.Pressed -= HandInput_Pressed;
            HandInput.Released -= HandInput_Released;
            GazeInput.GazeTargetChanged -= GazeInput_GazeTargetChanged;
        }

        private void HandInput_Pressed(object sender, EventArgs e)
        {
            OnPress();
        }

        private void HandInput_Released(object sender, EventArgs e)
        {
            OnRelease();
        }

        private void GazeInput_GazeTargetChanged(object sender, GazeInput.GazeEventArgs e)
        {
            HasFocus = e.GazeTarget == gameObject;
        }

        private void OnPress()
        {
            if (IsEnabled && HasFocus)
            {
                IsPressed = true;
                EventHandler temp = Pressed;
                if (temp != null) temp.Invoke(this, EventArgs.Empty);
            }
        }

        private void OnRelease()
        {
            if (IsPressed)
            {
                IsPressed = false;
                EventHandler temp = Released;
                if (temp != null) temp.Invoke(this, EventArgs.Empty);
            }
        }

#if UNITY_EDITOR
        private void OnMouseEnter()
        {
            HasFocus = true;
        }

        private void OnMouseExit()
        {
            HasFocus = false;
        }

        private void OnMouseDown()
        {
            OnPress();
        }

        private void OnMouseUp()
        {
            OnRelease();
        }
#endif
    }
}