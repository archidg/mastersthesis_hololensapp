﻿using System;
using UnityEngine;

namespace ArchiDemo.Inputs
{
    public class GazeInput : Singleton<GazeInput>
    {
        public class GazeEventArgs : EventArgs
        {
            public GameObject GazeTarget { get; private set; }

            public GazeEventArgs(GameObject gazeTarget)
            {
                GazeTarget = gazeTarget;
            }
        }

        public static GameObject GazeTarget { get; private set; }

        public static event EventHandler<GazeEventArgs> GazeTargetChanged;

        static GazeInput()
        {
            GazeTarget = null;
        }

        // Update is called once per frame
        void Update()
        {
            GameObject oldGazeTarget = GazeTarget;

            Vector3 headPosition = Camera.main.transform.position;
            Vector3 gazeDirection = Camera.main.transform.forward;

            RaycastHit hitInfo;

            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo))
            {
                transform.position = hitInfo.point;
                GazeTarget = hitInfo.transform.gameObject;
            }
            else
            {
                GazeTarget = null;
                transform.position = headPosition + 3 * gazeDirection;
            }

            EventHandler<GazeEventArgs> temp = GazeTargetChanged;
            if (GazeTarget != oldGazeTarget && temp != null) temp.Invoke(this, new GazeEventArgs(GazeTarget));
        }
    }
}