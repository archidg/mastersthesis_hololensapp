﻿using ArchiDemo.Inputs;
using UnityEngine;
using UnityEngine.Windows;
using System.Globalization;
using System.IO;
using File = UnityEngine.Windows.File;
using System;
using TMPro;

/** The smart objects have 'halo' effect as a component and a menu. 
 * TODO: For the 'User Configuration Mode' and 'Designer Configuration Mode' - the 
 * modules need to be arranged.
 */


namespace ArchiDemo
{
    public class Main : MonoBehaviour
    {
        // TODO: How to get the text after pressing a button and store it in a local csv file
        // TODO: Play a background narration like "welcome to demo" for the user to test the volume settings
        // TODO: Show a graph connecting smart devices that are associated with each activity-- if the user clicks on the activity

        // Use this for initialization
        void Start()
        {
            Lights.Activate();
            Origin.Activate();
            UserHand.Activate();
            HandInput.Activate();
            VoiceInput.Activate();
            //KinectInput.Activate(); //TestingInput.Activate(); //MenuCanvas.Activate(MenuCanvas.MenuType.MainMenu);
            //VoiceMessage.Play("Welcome to Nurse Aide", false);
            //VoiceMessage.Play("Welcome to the demo", false);   // The voice message helps explore the desired volume of the speakers

            SpeakerMenu.LowVolumeButtonClicked += SpeakerMenu_LowVolumeButtonClicked;
            SpeakerMenu.HighVolumeButtonClicked += SpeakerMenu_HighVolumeButtonClicked;
            SpeakerMenu.MediumVolumeButtonClicked += SpeakerMenu_MediumVolumeButtonClicked;

            
        }

        private void SpeakerMenu_MediumVolumeButtonClicked(object sender, EventArgs e)
        {
            String senderInfo = sender.ToString();
            //SpeakerMenu menu = (SpeakerMenu)sender;
            //menu.GetComponent(title)

            MenuButton mediumButton = ((SpeakerMenu)sender).GetComponent<MenuButton>();
            if (mediumButton != null) {
                TextMeshProUGUI buttonText = mediumButton.GetComponent<TextMeshProUGUI>();
                String text = buttonText.text;
                print("Information from the button is: " + senderInfo + text);
            }
           
            // mediumButton.ToString()
        }

        private void SpeakerMenu_HighVolumeButtonClicked(object sender, EventArgs e)
        {
            String senderInfo = sender.ToString();
            Debug.Log(senderInfo);
            print("Information from the button is: " + senderInfo);
        }

        private void SpeakerMenu_LowVolumeButtonClicked(object sender, System.EventArgs e)
        {
            // save the information from the button 
            // "https://docs.microsoft.com/en-us/windows/uwp/design/app-settings/store-and-retrieve-app-data"

            String senderInfo = sender.ToString();
            
            //// This example code can be used to read or write to an ApplicationData folder of your choice.
            //UnityEngine.Windows.Storage.StorageFolder localFolder = UnityEngine.Windows.Storage.ApplicationData.Current.LocalFolder;
            //// create datafile.txt in LocalFolder and write "My text" to it
            //StorageFile sampleFile = await localFolder.CreateFileAsync("dataFile.txt");
            //await FileIO.WriteTextAsync(sampleFile, "My text");

            ////Read the first line of dataFile.txt in LocalFolder and store it in a String
            //StorageFile sampleFile = await localFolder.GetFileAsync("dataFile.txt");
            //String fileContent = await FileIO.ReadTextAsync(sampleFile);

            print("SpeakerMenu_LowVolumeButtonClicked: " + senderInfo);
        }
    }
}

