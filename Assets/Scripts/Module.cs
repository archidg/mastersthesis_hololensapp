﻿using UnityEngine;

namespace ArchiDemo
{
    public class Module : MonoBehaviour
    {
        public ModuleType ModuleType;

        private void OnTriggerEnter(Collider other)
        {
            UserHand userHand = other.GetComponent<UserHand>();
            Foundation foundation = other.GetComponent<Foundation>();

            if (foundation != null && ModuleType == foundation.ModuleType
                || userHand != null && userHand.IsEmpty && transform.parent.GetComponent<Foundation>() == null)
            {
                transform.parent = other.transform;
                transform.localPosition = new Vector3();
                transform.localRotation = new Quaternion();
            }
        }
    }
}