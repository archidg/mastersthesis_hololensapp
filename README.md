# MastersThesis_HoloLenApp

* A Mixed-reality based reference implementation of Smart Built Environment (SBE) Design framework. 
* This application consists of two modes- "Design" and "Preview" mode. 
* The "Design" mode allows an SBE designer to explore multiple configurations of a modular architectural design. 
* The "Preview" mode allows designer/client to explore the interaction modalities like voice command at a real scale. 
* It also allows exploring the design at real scale on site.

